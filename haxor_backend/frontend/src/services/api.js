import axios from 'axios';

const API_BASE_URL = process.env.REACT_APP_API_BASE_URL || '/';

const api = {

  async getComics() {
    const result = await axios.get('/comics.json');
    return result.data;
  },

  async getSessionInfo(sessionId) {
    if (sessionId === null) {
      return null;
    }
    const result = await axios.get(API_BASE_URL + 'api/session/' + sessionId);
    return result.data;
  },

  async sendAnswer(sessionId, comicId, answer) {
    const url = API_BASE_URL + 'api/answer/' + (sessionId || '');
    const result = await axios.post(url, {
      'comic_id': comicId,
      'answer': Boolean(answer),
    });
    return result.data;
  },

  async deleteSession(sessionId) {
    // Disable deleting for now.
    //return await axios.delete(API_BASE_URL + 'api/session/' + sessionId);
  },

};

export default api;
