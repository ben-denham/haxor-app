import React, {useState, useEffect} from 'react';

const storage = window.localStorage;
const STORAGE_SESSION_ID_KEY = 'HAXOR_SESSION_ID';

function isUUID(value) {
  const uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
  return Boolean((value || '').match(uuidRegex));
}

function getHashSessionId() {
  const sessionId = window.location.hash.substring(1)
  return isUUID(sessionId) ? sessionId : null;
}

function setHashSessionId(sessionId) {
  if (sessionId) {
    window.location.hash = '#' + sessionId;
  }
  else {
    window.location.hash = '';
  }
}

function getSavedSessionId() {
  const sessionId = storage.getItem(STORAGE_SESSION_ID_KEY);
  return isUUID(sessionId) ? sessionId : null;
}

function setSavedSessionId(sessionId) {
  storage.setItem(STORAGE_SESSION_ID_KEY, sessionId);
}

function loadSessionId() {
  // The initial sessionId when the page loads should be retrieved
  // first from the hash, falling back to the saved session (ensuring
  // both are in sync after this function is called).
  const hashSessionId = getHashSessionId();
  if (hashSessionId !== null) {
    setSavedSessionId(hashSessionId);
    return hashSessionId;
  }
  const savedSessionId = getSavedSessionId();
  if (savedSessionId !== null) {
    setHashSessionId(savedSessionId);
    return savedSessionId;
  }
  return null;
}

export const SessionContext = React.createContext();

export function SessionProvider({children}) {
  const [sessionId, setSessionId] = useState(loadSessionId());

  function updateSessionId(sessionId) {
    if (sessionId !== null && !isUUID(sessionId)) {
      throw Error('Cannot set invalid sessionId.');
    }
    setSessionId(sessionId);
    setHashSessionId(sessionId);
    setSavedSessionId(sessionId);
  }

  function clearSession() {
    updateSessionId(null);
  }

  // Whenever the hash changes, update the sessionId.
  useEffect(function() {
    function handleHashChange() {
      updateSessionId(getHashSessionId());
    }
    window.addEventListener("hashchange", handleHashChange);
    return function cleanup() {
      window.removeEventListener("hashchange", handleHashChange);
    }
  }, []);

  return (
    <SessionContext.Provider
      value={{sessionId, updateSessionId, clearSession}}
    >
      {children}
    </SessionContext.Provider>
  );
}
