import React from 'react';
import ReactDOM from 'react-dom';

import {SessionProvider} from './services/sessions';
import App from './components/App';
import './index.css';

ReactDOM.render(
  (
    <SessionProvider>
      <App />
    </SessionProvider>
  ),
  document.getElementById('root')
);
