import React from 'react';
import {createUseStyles} from 'react-jss';

const useStyles = createUseStyles({
  frame: {
    margin: '5px auto',
    width: '85%',
    maxWidth: '780px',
    background: 'white',
    padding: '17px',
    border: '1.5px solid black',
    borderRadius: '12px',
    textAlign: 'center',
  },
});

function Frame({children}) {
  const classes = useStyles();
  return <div className={classes.frame}>{children}</div>;
}

export default Frame;
