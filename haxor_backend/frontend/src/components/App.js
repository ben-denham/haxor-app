import React, {useState, useEffect, useContext} from 'react';
import {createUseStyles} from 'react-jss';

import api from '../services/api';
import {SessionContext} from '../services/sessions';

import About from './About';
import Comic from './Comic';
import Frame from './Frame';
import Score from './Score';

const useStyles = createUseStyles({
  title: {
    fontSize: '2.5em',
  },
  subtitle: {
    fontSize: '0.6em',
  },
});

function App() {
  const classes = useStyles();
  const {sessionId, updateSessionId, clearSession} = useContext(SessionContext);
  const [sessionInfo, setSessionInfo] = useState(null);
  const [comics, setComics] = useState({});
  const [comic, setComic] = useState(null);

  function setRandomComic(comics, sessionInfo) {
    const all_comic_ids = Object.keys(comics);

    // Exclude comics the user has already viewed.
    const answers = ((sessionInfo || {}).answers || {})
    const viewed_ids = new Set(Object.keys(answers));
    var comic_ids = all_comic_ids.filter(id => !viewed_ids.has(id));

    // If the user has viewed all comics, pick any comic.
    if (comic_ids.length === 0) {
      comic_ids = all_comic_ids;
    }

    // Select a random comic.
    const randomIndex = Math.floor(Math.random() * comic_ids.length);
    const randomComicId = comic_ids[randomIndex];
    setComic(comics[randomComicId]);
  }

  useEffect(function() {
    const fetch = async function() {
      const comicsPromise = api.getComics();
      const sessionInfoPromise = api.getSessionInfo(sessionId);

      const comics = await comicsPromise;
      setComics(comics);
      const sessionInfo = await sessionInfoPromise;
      setSessionInfo(sessionInfo);

      // Set a random comic once data is loaded.
      setRandomComic(comics, sessionInfo);
      // If the session was not found, clear the user's session.
      if (sessionInfo === null) {
        clearSession();
      }
    }
    fetch();
  }, [sessionId]);

  async function answerHandler(comic_id, answer) {
    const newSessionInfo = await api.sendAnswer(sessionId, comic_id, answer);
    setSessionInfo(newSessionInfo);
    if (sessionId === null) {
      // If the user didnt' have a sessionId set previously, set their
      // session based on the response for their first answer.
      updateSessionId(newSessionInfo.session_id);
    }
    // Select a new comic.
    setRandomComic(comics, newSessionInfo);
  }

  async function clearSessionHandler() {
    await api.deleteSession(sessionId);
    setSessionInfo(null);
    clearSession();
  }

  return (
    <div className="app">
      <Frame>
        <h1 className={classes.heading + ' ' + classes.title}>
          Haxor Calculator
        </h1>
        <p>
          Haxor<sup>©</sup> (Hacker Appreciation of xkcd's Obscure
          References) is a totally useful metric of IT expertise
          calculated by comparing your understanding of tech jokes
          to that of fellow hackers.
        </p>
        <Score comics={comics} sessionInfo={sessionInfo} />
      </Frame>

      <Comic comic={comic} answerHandler={answerHandler} />

      <About clearSessionHandler={clearSessionHandler} />
    </div>
  );
}

export default App;
