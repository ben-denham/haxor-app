import React from 'react';
import {createUseStyles} from 'react-jss';
import {BlockMath} from 'react-katex';
import 'katex/dist/katex.min.css';

import Frame from './Frame';

const useStyles = createUseStyles({
  formula: {
    marginBottom: '1.2em',
    '@media (max-width: 630px)': {
      fontSize: '0.8em',
    },
    '@media (max-width: 520px)': {
      fontSize: '0.6em',
    },
    '@media (max-width: 390px)': {
      fontSize: '0.4em',
    },
  },
  clearSessionLink: {
    cursor: 'pointer',
  },
});

function About({clearSessionHandler}) {
  const classes = useStyles();

  return (
    <Frame>
      <h2 className={classes.heading}>About</h2>
      <div className={classes.formula}>
        <BlockMath>
          {String.raw`
          \begin{aligned}

          \text{Haxor} =
          \sum_{\text{comic}}^{\text{comics}}
          \text{understood}(\text{comic})
          -
          \frac{
          \text{users who understood comic}
          }{
          \text{users who viewed comic}
          } \\

          \text{understood}(\text{comic}) = \left\{\begin{matrix}
          1 & \text{if user did understand comic} \\
          0 & \text{if user did not understand comic}
          \end{matrix}\right.

          \end{aligned}
          `}
        </BlockMath>
      </div>
      <p>
        This definition means that the sum of all Haxors is zero
        (the proof is left as an exercise for the reader). This
        also makes the mean (not median) Haxor equal to zero,
        though the <em>real</em> average is probably a bit lower
        when accounting for dishonestly boosted Haxors.
      </p>
      <p>
        Bookmark this page to come back and view your updated HAXOR.
        Your session ID is saved in the URL, and is also saved in your
        browser's session
        (<a className={classes.clearSessionLink}
            onClick={clearSessionHandler}>clear session</a>).
      </p>
      <p>
        P.S. If you're a recruiter or manager and you think you may have
        found a magical metric of technical skill, you haven't. An
        engineer's expertise cannot be summed up in any kind of metric,
        let alone a joke one.
      </p>
      <p>
        All comics
        from <a href="https://xkcd.com">https://xkcd.com</a> used
        under Creative Commons Attribution-NonCommercial 2.5 License.
        This website is not associated with xkcd.
      </p>
      <p>
        Responses are stored anonymously for the purposes of score
        calculation and other analyses.
      </p>
    </Frame>
  );
}

export default About;
