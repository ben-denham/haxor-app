import React from 'react';
import {createUseStyles} from 'react-jss';

const useStyles = createUseStyles({
  noScore: {
    // This padding ensures the textbox is the same height as the
    // score (in most screen sizes).
    padding: '21px 0',
  },
  haxorScore: {
    fontSize: '2em',
    textDecoration: 'underline',
    margin: '5px 0',
  },
  haxorContext: {
    fontStyle: 'italic',
  },
});

function Score({comics, sessionInfo}) {
  const classes = useStyles();

  if (!sessionInfo) {
    return (
      <div className={classes.noScore}>
        <em>Read comics below to calculate your Haxor.</em>
      </div>
    );
  }
  else {
    const comicCount = Object.keys(comics || {}).length;
    const answerCount = Object.keys(sessionInfo.answers || {}).length;
    return (
      <div>
        <h3 className={classes.heading + ' ' + classes.haxorScore}>
          Your Haxor: {sessionInfo.haxor.toFixed(2)}
        </h3>
        <div className={classes.haxorContext}>
          (Based on {answerCount} {answerCount === 1 ? 'comic' : 'comics'} out
          of {comicCount})
        </div>
      </div>
    );
  }
}

export default Score;
