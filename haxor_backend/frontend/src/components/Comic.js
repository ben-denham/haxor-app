import React from 'react';
import {createUseStyles} from 'react-jss';

import Frame from './Frame';

const useStyles = createUseStyles({
  comicImage: {
    height: 'auto',
    maxWidth: '95%',
  },
  titleText: {
    padding: '0 5px',
  },
  button: {
    fontSize: '1.3em',
    padding: '0.5em',
    border: '2px solid',
    borderRadius: '6px',
    width: '40%',
    maxWidth: '150px',
    margin: '0 10px 10px 10px',
    cursor: 'pointer',
    fontWeight: 'bold',
  },
  yesButton: {
    background: '#3EFF4D',
    borderColor: '#37B848',
  },
  noButton: {
    background: '#FF2B21',
    borderColor: '#AD2A21',
  },
  externalLink: {
    backgroundImage: 'url(/images/external-link.svg)',
    backgroundPosition: 'center right',
    backgroundRepeat: 'no-repeat',
    backgroundSize: '10px',
    paddingRight: '13px',
  },
});

function ExtLink({href, children}) {
  const classes = useStyles();
  return (
    <a target="_blank" rel="noopener noreferrer"
       className={classes.externalLink} href={href}>
      {children}
    </a>
  );
}

function Comic({comic, answerHandler}) {
  const classes = useStyles();

  if (comic == null) {
    return null;
  }

  return (
    <Frame>
      <div>
        <button className={classes.button + ' ' + classes.noButton}
                onClick={() => answerHandler(comic.num, false)}>
          Huh?
        </button>
        <button className={classes.button + ' ' + classes.yesButton}
                onClick={() => answerHandler(comic.num, true)}>
          I get it!
        </button>
      </div>
      <h2 className={classes.heading}>{comic.title}</h2>
      <img className={classes.comicImage}
           src={comic.img}
           alt={comic.title}
           title={comic.alt} />
      <p className={classes.titleText}>Title text: "{comic.alt}"</p>
      <p className={classes.titleText}>
        <ExtLink href={'https://xkcd.com/' + comic.num}>
          Original comic
        </ExtLink>
        <span>&nbsp;|&nbsp;</span>
        <ExtLink href={'https://explainxkcd.com/wiki/index.php/' + comic.num}>
          Explain xkcd
        </ExtLink>
      </p>
    </Frame>
  );
}

export default Comic;
