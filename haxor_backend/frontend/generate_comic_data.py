import json
import os.path
import urllib.request

MAYBE_COMIC_IDS = [
    26, # Fourier
    55, # Useless
    69, # Pillow Talk
    74, # Su Doku
    91, # Pwned
    94, # Profile Creation Flowchart
    134, # Myspace
    184, # Matrix Transform
    190, # IPoD
    195, # Map of the Internet
    215, # Letting go
    272, # Linux User at Best Buy
    286, # All Your Base
    293, # RTFM
    341, # 1337: Part 1
    342, # 1337: Part 2
    344, # 1337: Part 4
    350, # Network
    364, # Responsible Behavior
    370, # Redwall
    380, # Emoticon
    407, # Cheap GPS
    504, # Legal Hacks
    518, # Flow charts
    528, # Windows 7
    612, # Estimation
    654, # Nachos
    662, # iPhone or Droid
    664, # Academia vs. Business
    694, # Retro Virus
    705, # Devotion to Duty
    743, # Infrastructures
    763, # Workaround
    801, # Golden Hammer
    814, # Diode
    844, # Good Code
    910, # Permanence
    932, # CIA
    936, # Password Strength
    949, # File Transfer
    1179, # ISO 8601
    1180, # Virus Venn Diagram
    1197, # All Adobe Updates
    1353, # Heartbleed
    1354, # Heartbleed Explanation
    1409, # Query
    1439, # Rack Unit
    1513, # Code Quality
    1646, # Twitter Bot
    1678, # Recent Searches
    1695, # Code Quality 2
    1726, # Unicode
    1760, # TV Problems
    1785, # Wifi
    1787, # Voice Commands
    1806, # Borrow Your Laptop
    1820, # Security Advice
    1822, # Existential Bug Reports
    1831, # Here to Help
    1833, # Supervillain Plan
    1909, # Digital Resource Lifespan
    1912, # Thermostat
    1938, # Meltdown and Spectre
    1953, # The History of Unicode
    2021, # Software Development
    2054, # Data Pipeline
    2102, # Internet Archive
    2116, # .NORM Normal File Format
    2138, # Wanna See the Code?
    2170, # Coordinate Precision
    2199, # Cryptic WiFi Networks
]

COMIC_IDS = [
    138, # Pointers
    149, # Sandwich
    153, # Cryptography
    156, # Commented
    163, # Donald Knuth
    173, # Movie Seating
    177, # Alice and Bob
    178, # Not Really Into Pokemon
    205, # Candy Button Paper
    208, # Regular Expressions
    217, # E to the Pi Minus Pi
    221, # Random Number
    224, # Lisp
    225, # Open Source
    234, # Escape Artist
    244, # Tabletop Roleplaying
    278, # Black Hat Support
    287, # NP-Complete
    292, # GOTO
    297, # Lisp Cycles
    303, # Compiling
    306, # Orphaned Projects
    312, # With Apologies to Robert Frost
    323, # Ballmer Peak
    327, # Exploits of a Mom
    329, # Turing Test
    340, # Fight
    349, # Success
    353, # Python
    371, # Compiler Complaint
    376, # Bug
    378, # Real Programmers
    379, # Forgetting
    394, # Kilobyte
    399, # Travelling Salesman Problem
    416, # Zealous Autoconfig
    424, # Security Holes
    426, # Geohashing
    434, # XKCD Goes to the Airport
    456, # Cautionary
    466, # Moving
    519, # 11th Grade
    530, # I'm an Idiot
    534, # Genetic Algorithms
    538, # Security
    554, # Not enough work
    571, # Can't sleep
    607, # 2038
    619, # Supported Features
    644, # Surgery
    645, # RPS
    676, # Abstraction
    686, # Admin Mourning
    720, # Recipes
    727, # Trade Expert
    742, # Campfire
    754, # Dependencies
    797, # Debian-Main
    816, # Applied Math
    835, # Tree
    838, # Incident
    865, # Nanobots
    869, # Server Attention Span
    912, # Manual Override
    953, # 1 to 10
    963, # X11
    1000, # 1000 Comics
    1033, # Formal Logic
    1084, # Server Problem
    1090, # Formal Languages
    1137, # RTL
    1144, # Tags
    1168, # Tar
    1171, # Perl Problems
    1172, # Workflow
    1181, # PGP
    1185, # Ineffective Sorts
    1188, # Bonding
    1209, # Encoding
    1210, # I'm So Random
    1234, # Douglas Engelbart (1925-2013)
    1247, # The Mother of All Suspicious Files
    1266, # Halting Problem
    1270, # Functional
    1275, # Int(Pi)
    1286, # Encryptic
    1296, # Git Commit
    1301, # File Extensions
    1306, # Sigil Cycle
    1312, # Haskell
    1313, # Regex Golf
    1323, # Protocol
    1341, # Types of Editors
    1361, # Google Announcement
    1381, # Margin
    1395, # Power Cord
    1421, # Future Self
    1481, # API
    1495, # Hard Reboot
    1508, # Operating Systems
    1537, # Types
    1553, # Public Key
    1597, # Git
    1605, # DNA
    1636, # XKCD Stack
    1638, # Backslashes
    1654, # Universal Install Script
    1667, # Algorithms
    1685, # Patch
    1692, # Man Page
    1700, # New Bug
    1718, # Backups
    1728, # Cron Mail
    1737, # Datacenter Scale
    1742, # Will It Work
    1755, # Old Days
    1764, # XKCDE
    1782, # Team Chat
    1790, # Sad
    1823, # Hottest Editors
    1833, # Code Quality 3
    1838, # Machine Learning
    1854, # Refresh Types
    1888, # Still In Use
    1897, # Self Driving
    1926, # Bad Code
    1957, # 2018 CVE List
    1960, # Code Golf
    1987, # Python Environment
    1988, # Containers
    2030, # Voting Software
    2044, # Sandboxing Cycle
    2099, # Missal of Silos
    2105, # Modern OSI Model
    2140, # Reinvent the Wheel
    2166, # Stack
    2173, # Trained a Neural Net
    2180, # Spreadsheets
    2200, # Unreachable State
    2210, # College Athletes
]

attributes = {'title', 'img', 'alt', 'num'}

import re
dodgy_regex = re.compile('.*(shit|fuck|sex).*', flags=re.I | re.DOTALL)
def read_comic(comic_id):
    print('Reading comic: {}'.format(comic_id))
    url = 'http://xkcd.com/{}/info.0.json'.format(comic_id)
    with urllib.request.urlopen(url) as comic:
        comic_data = json.loads(comic.read().decode())
        if dodgy_regex.match(comic_data['transcript']) or dodgy_regex.match(comic_data['alt']):
            print('Dodgy: ', comic_data['num'])
        comic = {k: v for k, v in comic_data.items() if k in attributes}
        # For consistency with numeric keys being turned into strings
        # by json.dump()
        comic['num'] = str(comic['num'])
        return comic

comics = {comic_id: read_comic(comic_id) for comic_id in COMIC_IDS}

script_path = os.path.dirname(os.path.abspath(__file__))
output_path = os.path.join(script_path, 'public', 'comics.json')
with open(output_path, 'w') as outfile:
    json.dump(comics, outfile)
