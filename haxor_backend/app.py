from datetime import datetime
from distutils.util import strtobool
from flask import Flask, abort, request, jsonify
import json
import os
import redis
from uuid import uuid4


app = Flask(__name__, static_url_path='', static_folder='frontend/build')
redis = redis.from_url(os.environ.get('REDIS_URL'))

with open('haxor_backend/frontend/public/comics.json') as comics_file:
    comics = json.load(comics_file)


def json_abort(code, message):
    response = jsonify({'error': message})
    response.status_code = code
    abort(response)


def increment_comic_stats(comic_id, answer, previous_answer):
    if comic_id not in comics:
        raise ValueError('Invalid comic_id: {}'.format(comic_id))

    if previous_answer is None:
        # If there was no previous_answer, increment the viewed counter
        redis.incr('comic:{}:viewed'.format(comic_id))
        if answer:
            # If the answer was True, also increment the understood counter
            redis.incr('comic:{}:understood'.format(comic_id))
    else:
        if answer and not previous_answer:
            # The user changed their answer from False to True
            redis.incr('comic:{}:understood'.format(comic_id))
        elif previous_answer and not answer:
            # The user changed their answer from True to False
            redis.decr('comic:{}:understood'.format(comic_id))


def get_comic_stats(comic_id):
    if comic_id not in comics:
        raise ValueError('Invalid comic_id: {}'.format(comic_id))
    # TODO: Should we get stats for all comics at once to improve
    # performance?
    stats = redis.mget(['comic:{}:viewed'.format(comic_id),
                        'comic:{}:understood'.format(comic_id)])
    return [ int(stat or 0) for stat in stats ]


def compute_haxor(answers):
    score = 0
    for comic_id, answer in answers.items():
        viewed_stat, understood_stat = get_comic_stats(comic_id)
        # Handle division by zero (though this shouldn't happen in practice)
        simplicity = (understood_stat / viewed_stat) if viewed_stat > 0 else 0
        understood = 1 if answer else 0
        score += understood - simplicity
    return score


def session_key(session_id):
    if not str(session_id):
        raise ValueError('Cannot build redis session key without session_id')
    return 'session:{}'.format(str(session_id))


def add_computed_session_fields(session):
    session['haxor'] = compute_haxor(session['answers'])
    return session


def get_session(session_id):
    session_json = redis.get(session_key(session_id)) or 'null'
    session = json.loads(session_json)
    if session is None:
        return None
    return add_computed_session_fields(session)


def generate_session_id():
    for i in range(100):
        session_id = str(uuid4())
        # Ensure the session_id hasn't been used previously
        if not redis.exists(session_key(session_id)):
            return session_id
    raise ValueError('Unable to generate a new unique session UUID.')


def save_session(session):
    allowed_keys = ['session_id', 'answers', 'created', 'updated']
    redis.set(session_key(session['session_id']),
              json.dumps({ key: session[key] for key in allowed_keys }))


def delete_session(session_id):
    redis.delete(session_key(session_id))

# Routes

@app.route('/', methods=['GET'])
def index():
    return app.send_static_file('index.html')


@app.route('/api/session/<uuid:session_id>',
           # Disable deletion for now.
           #methods=['GET', 'DELETE']
           methods=['GET']
)
def session(session_id):
    if request.method == 'GET':
        return jsonify(get_session(session_id))
    elif request.method == 'DELETE':
        delete_session(session_id)
        return jsonify({'deleted': session_id})


@app.route('/api/answer/', methods=['POST'])
@app.route('/api/answer/<uuid:session_id>', methods=['POST'])
def answer_session(session_id=None):
    if not request.json:
        json_abort(400, 'No JSON data received.')

    required_keys = ['answer', 'comic_id']
    for key in required_keys:
        if key not in request.json:
            json_abort(400, '{} key is required'.format(key))

    answer = bool(request.json['answer'])
    comic_id = str(request.json['comic_id'])
    if comic_id not in comics:
        json_abort(400, 'Invalid comic_id: {}'.format(comic_id))

    previous_answer = None
    now = datetime.utcnow().isoformat()
    if session_id is None:
        # New session
        session_id = generate_session_id()
        session = {
            'session_id': session_id,
            'answers': {comic_id: answer},
            'created': now,
            'updated': now,
        }
    else:
        # Existing session
        session = get_session(session_id)
        if session is None:
            json_abort(404, 'Unknown session_id: {}'.format(session_id))
        if comic_id in session['answers']:
            previous_answer = session['answers'][comic_id]
        session['answers'][comic_id] = answer
        session['updated'] = now

    save_session(session)
    increment_comic_stats(comic_id, answer, previous_answer)
    return jsonify(add_computed_session_fields(session))


if __name__ == '__main__':
    cors_origin = os.environ.get('BACKEND_CORS_ORIGIN', None)
    if cors_origin:
        from flask_cors import CORS
        CORS(app, resources={r"/api/*": {"origins": cors_origin}})
    app.debug = strtobool(os.environ.get('BACKEND_DEBUG', 'false'))
    app.run(
        host=os.environ.get('BACKEND_HOST', '127.0.0.1'),
        port=int(os.environ.get('BACKEND_PORT', '5000'))
    )
